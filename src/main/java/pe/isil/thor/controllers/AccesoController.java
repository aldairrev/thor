package pe.isil.thor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import pe.isil.thor.models.Usuario;

import java.util.Map;

@Controller
@RequestMapping("/login")
public class AccesoController {

    @Autowired
    private RestTemplate restTemplate;

    private static final String GET_ACCESO_JWT_API = "http://localhost:7000/acceso";
    private static final String GET_SALUDO_JWT_API = "http://localhost:7000/hola";

    @GetMapping("")
    public String login(Model model){
        model.addAttribute("usuario", new Usuario());
        return "login";
    }

    @PostMapping("")
    public String token_acceso(Model model,
                        @RequestParam("nombreUsuario") String nombreUsuario,
                        @RequestParam("contrasena") String contrasena){
        
        //Configuracion para el request
        HttpHeaders headers_acceso = new HttpHeaders();
        headers_acceso.setContentType(MediaType.APPLICATION_JSON);
        Usuario u = new Usuario();
        u.setNombreUsuario(nombreUsuario);
        u.setContrasena(contrasena);
        HttpEntity<Usuario> httpEntity_acceso = new HttpEntity<>(u, headers_acceso);

        //Autorizacion
        Usuario usuario = restTemplate.postForObject(GET_ACCESO_JWT_API, httpEntity_acceso, Usuario.class);

        System.out.println(usuario.getToken());
        //Creacion headers
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", usuario.getToken());

        //Creacion de request
        HttpEntity request = new HttpEntity(httpHeaders);

        //Consumir el API JWT enviando el request
        ResponseEntity<String> response = new RestTemplate().exchange(GET_SALUDO_JWT_API, HttpMethod.GET, request, String.class);

        //Obtener JSON response
        String msje = response.getBody();

        model.addAttribute("msje", msje);
        return "mensaje";
    }

}
